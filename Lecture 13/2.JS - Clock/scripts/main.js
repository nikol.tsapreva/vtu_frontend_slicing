function showCurrentTime() {
  const timeEl = document.getElementById("time");
  const now = new Date();
  const hour = now.getHours();
  const min = now.getMinutes();
  const sec = now.getSeconds();
  const display = hour + ":" + min + ":" + sec;

  timeEl.innerHTML = display;
}

function showCurrentDay() {
  const dateEl = document.getElementById("date");
  const now = new Date();
  const day = now.getDay();
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  dateEl.innerHTML = days[day];
}

showCurrentTime();
showCurrentDay();

setInterval(function () {
  showCurrentTime();
}, 1000);
