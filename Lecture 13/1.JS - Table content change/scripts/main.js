function changeContent() {
  const rowInput = document.getElementById("row");
  const rowVal = parseInt(rowInput.value);
  const rowError = document.getElementById("row-error");
  const colInput = document.getElementById("column");
  const colVal = parseInt(colInput.value);
  const colErr = document.getElementById("column-error");
  const contentVal = document.getElementById("content").value;
  let error = false;

  if (isNaN(rowVal) || rowVal < 0 || rowVal > 2) {
    error = true;
    rowInput.style.borderColor = "red";
    rowError.innerHTML = "Enter a number between 0 and 2.";
  } else {
    rowInput.style.borderColor = "black";
    rowError.innerHTML = "";
  }

  if (isNaN(colVal) || colVal < 0 || colVal > 1) {
    error = true;
    colInput.style.borderColor = "red";
    colErr.innerHTML = "Enter a number between 0 and 1.";
  } else {
    colInput.style.borderColor = "black";
    colErr.innerHTML = "";
  }

  if (error) {
    return;
  }

  const successMsgEl = document.createElement("p");
  const x = document.getElementById("myTable").rows[rowVal].cells;

  x[colVal].innerHTML = contentVal;
  rowInput.value = "";
  colInput.value = "";
  content.value = "";

  successMsgEl.innerHTML = "The content has been succesfully changed.";
  successMsgEl.style.cssText =
    "background-color: green; color: white; padding: 20px;";
  document.body.appendChild(successMsgEl);

  setTimeout(function () {
    document.body.removeChild(successMsgEl);
  }, 3000);
}
