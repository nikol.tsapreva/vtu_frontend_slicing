// Достъпваме елемента #text по id
const element = document.getElementById("text");

// Добавяме съдържание (текст), цвят и размер на шрифта
element.innerText = "Hello!";
element.style.color = "green";
element.style.fontSize = "28px";

// Достъпваме елементите по клас .number, добавяме !!! след текста и background-color: red
const numberElements = document.getElementsByClassName("number");
for (let i = 0; i < numberElements.length; i++) {
  const numberEl = numberElements[i];
  numberElements[i].innerHTML += "!!!";
  numberElements[i].style.backgroundColor = "red";
}

// Създаване на DOM елемент бутон и го добавяме към #button-wrapper
const buttonWrapperEl = document.getElementById("button-wrapper");
const btn = document.createElement("button");
btn.innerHTML = "Click";
buttonWrapperEl.appendChild(btn);
