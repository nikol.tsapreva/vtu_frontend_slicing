// Използвайки if-else: Часът е 10 сутринта. Какъв е поздравът през различните части от деня?
const time = 10;
let greeting;

if (time < 10) {
  greeting = "Good morning";
} else if (time < 20) {
  greeting = "Good day";
} else {
  greeting = "Good evening";
}

console.log(greeting);

// Използвайки switch: Проверете кой ден от седмицата е dayNumber.
const dayNumber = 4;
let dayName;

switch (dayNumber) {
  case 0:
    dayName = "Sunday";
    break;
  case 1:
    dayName = "Monday";
    break;
  case 2:
    dayName = "Tuesday";
    break;
  case 3:
    dayName = "Wednesday";
    break;
  case 4:
    dayName = "Thursday";
    break;
  case 5:
    dayName = "Friday";
    break;
  case 6:
    dayName = "Saturday";
    break;
  default:
    dayName = "Not part of the week.";
}

console.log(dayName);

// Ternary operator
const hours = 5;
const hourString = hours < 10 ? "0" + hours : hours.toString();
console.log(hourString);
