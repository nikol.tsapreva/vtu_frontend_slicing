// Типове данни
// Primitive types: null, undefined, string, boolean, number
// Reference types: functions, arrays, object - от "Object"

let x = 5;
let lastName = "Ivanov";
let isActive = true;
let userData = { name: "Ivan", age: 18 };
let cars = ["Audi", "VW", "Fiat"];

// typeOf операторът връща типа на променливата
console.log(typeof userData);

// Опасност от загубване на типове данни
let johnTaskCount = 11;
let janeTaskCount = "42";
let totalTaskCount = johnTaskCount + janeTaskCount;
console.log(totalTaskCount); // "1142" - string
