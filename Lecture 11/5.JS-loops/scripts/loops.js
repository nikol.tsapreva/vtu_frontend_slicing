// Изведете в конзолата числата от 0 до 80 включително.

// Използвайки for
for (let i = 0; i <= 80; i++) {
  console.log(i);
}

// Използвайки while
let y = 0;
while (y <= 80) {
  console.log(y);
  y++;
}

// Използвайки do-while
let z = 0;
do {
  console.log(z);
  z++;
} while (z <= 80);

// Изведете в конзолата четните числа от 1 до 50, използвайки for и if
for (let index = 1; index <= 50; index++) {
  if (index % 2 == 0) {
    console.log(index);
  }
}

// Изведете числата от 50 до 1, използвайки for
for (let index = 50; index > 0; index--) {
  console.log(index);
}

// Изведете числата от 10 до 0, които се делят на 2 и 3 без остатък, използвайки for и if
for (let index = 10; index > 0; index--) {
  if (index % 2 === 0 && index % 3 === 0) {
    console.log(index);
  }
}
