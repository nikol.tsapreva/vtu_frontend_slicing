let x, y, sum; // Деклариране на променливи
x = 10; // Присвояване на стойност
y = 20;
sum = x + y; // Оператор за събиране и присвояване на стойността

console.log("The sum of x and y is " + sum);

// Разлика между var и let - обхват (scope)
{
  var firstName = "Ivan";
  let fatherName = "Draganov";
  console.log(fatherName);
}

console.log(firstName);
console.log(fatherName); // Грешка - изпълнението на програмата спира тук

// Const - константа
const egn = 9709121420;
console.log(egn);
egn = 10; // Грешка
