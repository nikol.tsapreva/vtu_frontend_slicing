// Използвайки innerHTML в/у HTML елемент
document.getElementById("paragraph").innerHTML = "Съдържанието е променено чрез innerHTML.";

// Използвайки document.write() - този метод е препоръчително единствено да се използва за тестови цели
document.write("Използваме document.write()");

// Използвайки alert (document.alert())
alert("Зареждане...");

// Използвайки console.log() - обикновено се използва при дебъгване
console.log("Сумата на 5 + 5 е: ", 5 + 5);