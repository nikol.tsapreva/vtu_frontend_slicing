// Click events
function openAlert(name) {
  alert("Hi " + name + "!");
}

function sayHi() {
  const textElement = document.getElementById("text");
  const inputElement = document.getElementById("name-input");
  
  textElement.innerText = "Hello " + inputElement.value;
  inputElement.value = "";
}

function decrease() {
  const numberEl = document.getElementById("number");
  let val = parseInt(numberEl.innerHTML);
  val--;
  numberEl.innerHTML = val;
}

function increase() {
  const numberEl = document.getElementById("number");
  let val = parseInt(numberEl.innerHTML);
  val++;
  numberEl.innerHTML = val;
}

// Accordion
let isExpanded = false;
function toggle() {
  const extraDiv = document.getElementById("extra");
  const toggleBtn = document.getElementsByClassName("toggle-btn")[0];

  if (isExpanded) {
    extraDiv.style.display = "block";
    toggleBtn.textContent = "Show Less";
  } else {
    extraDiv.style.display = "none";
    toggleBtn.textContent = "Show More";
  }

  isExpanded = !isExpanded;
}
toggle();

// Mouse events
const dynamicEl = document.getElementsByClassName("dynamic")[0];

function mOver() {
  dynamicEl.innerHTML = "Mouse Over Me Done";
}

function mOut() {
  dynamicEl.innerHTML = "Mouse Over Me";
}
